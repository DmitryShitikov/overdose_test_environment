#!/bin/bash

set -e

file="/etc/ssl/certs/myssl.crt"
if [[ ! -f $file ]]; then
    curdir=$PWD
    cd /tmp/certs
    file="myssl.crt"
    if [[ ! -f $file ]]; then
        openssl req -batch -new -newkey rsa:4096 -days 365 -nodes -x509 -subj "/C=UA/ST=Dnipro/L=Dnipro/O=Dis/CN=Dnipro" -keyout myssl.key -out myssl.crt
    fi
    mkdir -p  /etc/ssl/certs
    cp myssl.crt /etc/ssl/certs/
    mkdir -p  /etc/ssl/private
    cp myssl.key /etc/ssl/private/
    cd $curdir
fi

sed -i 's/{SERVER_NAME}/'"$SERVER_ALIASES_APP"'/g' /etc/nginx/sites-enabled/web-01

exec "$@"