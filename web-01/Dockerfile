FROM ubuntu:16.04

RUN apt-get update &&  \
    apt-get install -y \
      vim \
      nano \
      less \
      wget \
      ssmtp \
      unzip \
      make \
      nginx \
      python-software-properties \
      software-properties-common && \
    LC_ALL=C.UTF-8 add-apt-repository -y ppa:ondrej/php && \
    apt-get update && \
    apt-get install -y \
      php7.3 \
      php7.3-dev \
      php7.3-xdebug \
      php7.3-bcmath \
      php7.3-fpm \
      php7.3-common \
      php7.3-ctype \
      php7.3-curl \
      php7.3-dom \
      php7.3-gd \
      php7.3-opcache \
      php7.3-iconv \
      php7.3-intl \
      php7.3-mbstring \
      php7.3-mysql \
      php7.3-xml \
      php7.3-xsl \
      php7.3-zip \
      php7.3-soap \
      php-pear \
      libxml2 \
      libxml2-dev

RUN mkdir -p /var/www/app
COPY xdebug.ini /etc/php/7.3/mods-available/
COPY magento2-cli.ini /etc/php/7.3/cli/conf.d/
COPY magento2-fpm.ini /etc/php/7.3/fpm/conf.d/

RUN apt-get update &&  \
    apt-get install -y \
      sed \
      redis-tools \
      php7.3-zip \
      screen

EXPOSE 80
EXPOSE 9000

COPY docker-entrypoint.sh /usr/local/bin/
RUN chmod +x /usr/local/bin/docker-entrypoint.sh

COPY init-php-fpm.sh /usr/local/bin/
RUN chmod +x /usr/local/bin/init-php-fpm.sh

COPY install-composer.sh /usr/local/bin/
RUN chmod +x /usr/local/bin/install-composer.sh

RUN apt-get install curl -y

RUN apt-get update && \
    apt-get install -y --no-install-recommends git zip

RUN curl --silent --show-error https://getcomposer.org/installer | php

COPY sites-enabled/* /etc/nginx/sites-enabled

ARG UID=1000
ARG GID=1000
ENV UID=${UID}
ENV GID=${GID}
RUN usermod -u $UID www-data \
  && groupmod -g $GID www-data

RUN mkdir /tmp/certs

ENTRYPOINT ["docker-entrypoint.sh"]

WORKDIR /var/www/app

STOPSIGNAL SIGTERM

CMD ["nginx", "-g", "daemon off;"]