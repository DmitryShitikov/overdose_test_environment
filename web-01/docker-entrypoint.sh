#!/bin/bash

set -e

sh /usr/local/bin/init-php-fpm.sh

file="/etc/ssl/certs/myssl.crt"
if [[ ! -f $file ]]; then
    curdir=$PWD
    cd /tmp/certs
    file="myssl.crt"
    if [[ ! -f $file ]]; then
        openssl req -batch -new -newkey rsa:4096 -days 365 -nodes -x509 -subj "/C=UA/ST=Dnipro/L=Dnipro/O=Dis/CN=Dnipro" -keyout myssl.key -out myssl.crt
    fi
    mkdir -p  /etc/ssl/certs
    cp myssl.crt /etc/ssl/certs/
    mkdir -p  /etc/ssl/private
    cp myssl.key /etc/ssl/private/
    cd $curdir
fi

echo "max_input_vars = 4000" >> /etc/php/7.3/cli/php.ini

echo "alias composer='php -d memory_limit=2G /composer.phar'" >>  /root/.bashrc

echo "alias magento='php -d memory_limit=2G /var/www/app/bin/magento'" >>  /root/.bashrc

exec "$@"